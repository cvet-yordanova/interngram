import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Post } from '../../../models/post.model';
import { Router, ActivatedRoute } from '@angular/router';
import { PostsService } from '../../posts.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-posts-single',
  templateUrl: './posts-single.component.html',
  styleUrls: ['./posts-single.component.css']
})
export class PostsSingleComponent {

  @Input() post: Post;
  @Output() open: EventEmitter<any> = new EventEmitter();

  constructor(private postsService: PostsService,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService) { }

  onEditPost(): void {
    this.router.navigate([this.post.id, 'edit'], { relativeTo: this.route });
  }

  onView(): void {
    this.open.emit(this.post);
  }

  onDeletePost(): void {
    let id = +this.post.id;
    if (confirm('Are you sure you want to delete this?')) {
      this.postsService.deletePost(id).subscribe(() => {
        let post = this.postsService.postsChanged
          .getValue()
          .filter(a => a.id == id)[0];
        let idx = this.postsService.postsChanged
          .getValue()
          .indexOf(post);
        this.postsService.postsChanged
          .getValue()
          .splice(idx, 1);
        this.toastr.success("Post deleted", "Success!", {
          timeOut: 2000
        });
      });
    }
  }

  getBackground(): string {
    let background = '';
    switch (this.post.type) {
      case 'IMAGE':
        background = this.post.meta.url ? this.post.meta.url :
          'http://www.stleos.uq.edu.au/wp-content/uploads/2016/08/image-placeholder-350x350.png';
        break;
      case 'VIDEO':
        background = this.post.meta.thumbnail ?
          this.post.meta.thumbnail :
          'https://media.idownloadblog.com/wp-content/uploads/2016/10/youtube.jpg';
        break;
      case 'LINK':
        background = 'https://www.gstatic.com/images/branding/product/1x/google_cloud_search_512dp.png';
      default: '';
    }

    return background;
  }

}
