import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post.model';
import { PostsService } from '../posts.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-posts-list',
  templateUrl: './posts-list.component.html',
  styleUrls: ['./posts-list.component.css']
})
export class PostsListComponent implements OnInit {

  postsNormal = new BehaviorSubject([]);
  showDetails = false;
  loading = false;

  viewPost: Post;

  constructor(private postsService: PostsService) { }

  ngOnInit(): void {
    this.loading = true;
    this.postsService.getPostsScroll();
    this.postsNormal = this.postsService.postsChanged;
    this.loading = false;
  }

  onScroll(): void {
    this.loading = true;
    this.postsService.getPostsScroll();
    this.loading = false;
  }

  opened(post): void {

    if (post.type == 'LINK') {
      window.open(post.meta.url, '_blank');
      return;
    }
    this.viewPost = post;
    this.showDetails = true;
  }

  closed(): void {
    this.showDetails = false;
  }

}
