import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Post } from '../../../models/post.model';

@Component({
  selector: 'app-posts-image',
  templateUrl: './posts-image.component.html',
  styleUrls: ['./posts-image.component.css']
})
export class PostsImageComponent {

  @Output() close: EventEmitter<any> = new EventEmitter();
  @Input() post: Post;

  constructor() { }

  onClose(): void {
    this.close.emit(null);
  }

}
