import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsVideoComponent } from './posts-video.component';

describe('PostsVideoComponent', () => {
  let component: PostsVideoComponent;
  let fixture: ComponentFixture<PostsVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PostsVideoComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
