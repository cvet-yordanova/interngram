import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { Post } from '../../../models/post.model';

@Component({
  selector: 'app-posts-video',
  templateUrl: './posts-video.component.html',
  styleUrls: ['./posts-video.component.css']
})
export class PostsVideoComponent implements OnInit {

  @Output() close: EventEmitter<any> = new EventEmitter();
  @Input() post: Post;
  @ViewChild('iframe') iframe: ElementRef;

  loaded: boolean;

  constructor() { }

  ngOnInit(): void {
    this.iframe
      .nativeElement
      .setAttribute('src', this.post.meta.videoLinkEmbed ?
        this.post.meta.videoLinkEmbed : '');
  }

  onClose(): void {
    this.close.emit();
  }

}
