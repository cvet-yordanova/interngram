import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, BehaviorSubject } from "rxjs";
import { Post } from "../models/post.model";

@Injectable({
    providedIn: 'root'
})
export class PostsService {


    ALL_POSTS_URL: string = 'http://localhost:3000/posts';
    SINGLE_POST_URL: string = 'http://localhost:3000/posts/';
    HEADERS: { [key: string]: string } = { 'Content-Type': 'application/json' };


    postsChanged = new BehaviorSubject<Post[]>([]);
    page = 1;
    lastPage = false;

    constructor(
        private http: HttpClient) {
    }

    getPostsScroll(): void {

        this.http.get<Array<Post>>(this.ALL_POSTS_URL, {
            params: {
                _page: `${this.page}`,
                _sort: 'id',
                _order: 'desc'
            }
        }).subscribe(result => {

            if (this.lastPage) {
                return;
            }

            if (result.length < 10) {
                this.lastPage = true;
            }

            if (result.length >= 10) {
                this.page++;
            }

            this.postsChanged.next(this.postsChanged.getValue().concat(result));
        });
    }


    getPostById(id: number): Observable<Post> {
        return this.http
            .get<Post>(this.SINGLE_POST_URL + id);
    }

    updatePost(id: number, updatedPost: Post): Observable<Post> {
        return this.http
            .patch<Post>(this.SINGLE_POST_URL + id, updatedPost, { headers: this.HEADERS });
    }

    createPost(post: Post): Observable<Post> {
        return this.http
            .post<Post>(this.SINGLE_POST_URL, post, { headers: this.HEADERS });

    }

    deletePost(id: number): Observable<{}> {
        return this.http.delete<{}>(this.SINGLE_POST_URL + id);
    }

}
