import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { PostsService } from '../posts.service';
import { Post } from '../../models/post.model';
import { FormGroup, FormControl, Validators, AbstractControl } from '@angular/forms';
import { YoutubeService } from '../../shared/youtube-service';
import { Meta } from '../../models/meta.model';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-posts-edit',
    templateUrl: './posts-edit.component.html',
    styleUrls: ['./posts-edit.component.css']
})
export class PostsEditComponent implements OnInit {

    postForm: FormGroup;
    post: Post;
    postMeta: Meta;
    id: number;

    editMode: boolean = false;

    videoId: string;
    videoLink: string;
    videoLinkEmbed: string;
    thumbImage: string;
    alt: string;

    @ViewChild('videoElement') videoElement: ElementRef;
    @ViewChild('type') typeElement: ElementRef;
    @ViewChild('url') linkElement: ElementRef;
    @ViewChild('alt') altElement: ElementRef;
    @ViewChild('imgPreview') imgPreview: ElementRef;

    constructor(
        private route: ActivatedRoute,
        private postsService: PostsService,
        private router: Router,
        private youtubeService: YoutubeService,
        private toastr: ToastrService
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe((params: Params) => {
            this.id = +params['id'];
            this.editMode = params['id'] != null;
            this.initForm()
        });
    }

    onCancel(): void {
        this.router.navigate(['posts']);
    }

    onKey(): void {
        this.updateLinks();
    }

    onChange(): void {
        this.updateLinks();
    }

    onSubmit(): void {
        if (this.editMode) {
            this.procedeOnEdit();
            this.router.navigate(['posts']);
        } else {
            this.procedeOnCreate();
            this.router.navigate(['/posts']);
        }
    }

    get f(): { [key: string]: AbstractControl; } { return this.postForm.controls }

    private createFormGroup(postType: string, postTitle: string, url: string, alt?: string): void {
        this.postForm = new FormGroup({
            title: new FormControl(postTitle, Validators.required),
            type: new FormControl(postType, Validators.required),
            postMetaUrl: new FormControl(url),
            postMetaAlt: new FormControl(alt)
        });
    }

    private initForm(): void {
        let postType = '';
        let postTitle = '';
        let postMetaUrl = '';
        let postMetaAlt = '';

        if (this.editMode) {
            this.postsService
                .getPostById(this.id)
                .subscribe((data) => {
                    this.post = data;
                    this.postMeta = this.post.meta;

                    postTitle = this.post.title;
                    postMetaUrl = this.postMeta.url ? this.postMeta.url : '';
                    postMetaAlt = this.postMeta.alt ? this.postMeta.alt : '';
                    postType = this.post.type;

                    this.setPreviewLinks();
                    this.createFormGroup(postType, postTitle, postMetaUrl, postMetaAlt)
                    return;
                });
        }

        this.createFormGroup(postType, postTitle, postMetaUrl, postMetaAlt);
    }



    private procedeOnCreate(): void {
        let formValue = this.postForm.value;

        let meta = new Meta(
            formValue.postMetaUrl,
            formValue.postMetaAlt ? formValue.postMetaAlt : null,
            this.thumbImage ? this.thumbImage : null,
            this.videoLink ? this.videoLink : null,
            this.videoLinkEmbed ? this.videoLinkEmbed : null
        );

        let newPost = new Post(formValue.type,
            formValue.title, meta);

        this.postsService.createPost(newPost)
            .subscribe((post) => {
                this.postsService.postsChanged
                    .getValue()
                    .unshift(post);
                this.toastr.success("Post created!", "Success!", {
                    timeOut: 2000
                });
            });
        this.resetFields();
        this.router.navigate(['/posts']);
    }

    private procedeOnEdit(): void {
        this.postsService.getPostById(this.id).subscribe(resultPost => {
            let postData = this.postForm.value;

            resultPost.title = postData.title;
            resultPost.type = postData.type;
            resultPost.meta.url = postData.postMetaUrl;
            resultPost.meta.alt = postData.postMetaAlt;
            resultPost.meta.videoLinkEmbed = this.videoLinkEmbed;
            resultPost.meta.thumbnail = this.thumbImage;

            this.postsService.updatePost(this.id, resultPost)
                .subscribe(() => {
                    let post = this.postsService.postsChanged
                        .getValue()
                        .filter(a => a.id == resultPost.id)[0];
                    let idx = this.postsService.postsChanged
                        .getValue()
                        .indexOf(post);
                    this.postsService.postsChanged
                        .getValue()
                        .splice(idx, 1, resultPost);
                    this.toastr.success("Post updated", "Success!", {
                        timeOut: 2000
                    });

                }, () => {
                    this.toastr.error("Error", "Error", {
                        timeOut: 2000
                    });
                });
        });
    }

    private updateLinks(): void {
        let linkValueNative = this.linkElement.nativeElement;

        switch (this.typeElement.nativeElement.value) {
            case 'IMAGE':
                this.thumbImage = linkValueNative.value ?
                    linkValueNative.value : '';
                break;
            case 'VIDEO':
                this.videoId = this.youtubeService
                    .getVideoId(linkValueNative.value);

                this.thumbImage = this.youtubeService
                    .getDefaultYoutubeImg();
                this.videoLinkEmbed = null;

                this.youtubeService
                    .checkIfVideoExists(this.videoId)
                    .subscribe(data => {
                        if (data.pageInfo.totalResults > 0) {
                            this.thumbImage = this.youtubeService
                                .getVideoThumbnail(this.videoId);
                            this.videoLink = this.youtubeService
                                .getVideoUrlWatch(this.videoId);
                            this.videoLinkEmbed = this.youtubeService
                                .getVideoUrlEmbed(this.videoId);
                        } else {
                            this.thumbImage = this.youtubeService
                                .getDefaultYoutubeImg();
                            this.videoLink = null;
                            this.videoLinkEmbed = null;
                        }
                    });
                break;
            case 'LINK':
                break;
            default: this.thumbImage = null;
        }
    }

    private resetFields(): void {
        this.thumbImage = '';
        this.videoLink = '';
        this.videoLinkEmbed = '';
    }


    private setPreviewLinks(): void {
        switch (this.post.type) {
            case 'IMAGE':
                this.thumbImage = this.postMeta.url ?
                    this.postMeta.url : '';
                break;
            case 'VIDEO':
                this.thumbImage = this.postMeta.thumbnail ?
                    this.postMeta.thumbnail :
                    this.youtubeService.getDefaultYoutubeImg();

                this.videoLinkEmbed = this.postMeta.videoLinkEmbed ?
                    this.postMeta.videoLinkEmbed : '';

                this.videoLink = this.postMeta.videoLink ?
                    this.postMeta.videoLink : '';
                break;
        }
    }

}
