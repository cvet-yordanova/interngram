import { Routes, RouterModule, ActivatedRouteSnapshot } from '@angular/router';
import { NgModule, InjectionToken } from '@angular/core';
import { PostsListComponent } from './posts/posts-list/posts-list.component';
import { PostsEditComponent } from './posts/posts-edit/posts-edit.component';

const appRoutes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'posts' },
    {
        path: 'posts', component: PostsListComponent
    },
    { path: 'posts/:id/edit', component: PostsEditComponent },
    { path: 'posts/add', component: PostsEditComponent }
  
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }