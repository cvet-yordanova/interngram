import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app.routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoadingSpinnerComponent } from './shared/loading-spinner/loading-spinner.component';
import { PostsListComponent } from './posts/posts-list/posts-list.component';
import { PostsSingleComponent } from './posts/posts-list/posts-single/posts-single.component';
import { PostsEditComponent } from './posts/posts-edit/posts-edit.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ToastrModule, ToastContainerModule} from 'ngx-toastr';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {MatIconModule, MatDatepickerModule, MatNativeDateModule} from '@angular/material';
import { PostsImageComponent } from './posts/posts-list/posts-image/posts-image.component';
import { PostsVideoComponent } from './posts/posts-list/posts-video/posts-video.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoadingSpinnerComponent,
    PostsListComponent,
    PostsSingleComponent,
    PostsEditComponent,
    PostsImageComponent,
    PostsVideoComponent
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    InfiniteScrollModule,
    MatIconModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ToastrModule.forRoot()
  ],
  exports: [
    MatDatepickerModule,
    MatNativeDateModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
