export class Meta {
    url: string;
    alt: string;
    thumbnail: string;
    videoLink: string;
    videoLinkEmbed: string;

    constructor(url: string,
        alt: string,
        thumbnail: string,
        videoLink,
        videoLinkEmbed: string
    ) {
        this.url = url;
        this.alt = alt;
        this.thumbnail = thumbnail;
        this.videoLink = videoLink;
        this.videoLinkEmbed = videoLinkEmbed;
    }
}